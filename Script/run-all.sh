cd /root/all
#####################################################################################################################
echo "Create Pod for Througput and Packet Loss test"

kubectl create -f iperf3.yaml

echo "Create Pod for Latency"

kubectl create -f sockperf.yaml

until $(kubectl get pods -l app=iperf3-client -o jsonpath='{.items[0].status.containerStatuses[0].ready}'); do
    echo "Waiting for iperf3-client to start..."
    sleep 5
done

sleep 5

until $(kubectl get pods -l app=iperf3-server -o jsonpath='{.items[0].status.containerStatuses[0].ready}'); do
    echo "Waiting for iperf3 server to start..."
    sleep 5
done

echo "Pod iperf is running"
#####################################################################################################################

until $(kubectl get pods -l app=sockperf-client -o jsonpath='{.items[0].status.containerStatuses[0].ready}'); do
    echo "Waiting for sockperf-client server to start..."
    sleep 5
done

until $(kubectl get pods -l app=sockperf-server -o jsonpath='{.items[0].status.containerStatuses[0].ready}'); do
    echo "Waiting for sockperf-server server to start..."
    sleep 5
done

echo "Pod sockperf is running"

#####################################################################################################################
/bin/bash ./script-iperf3.sh

sleep 10
#####################################################################################################################
/bin/bash ./script-sockperf.sh

#echo "Done test Latency"
#echo "Delete Pod"
#kubectl delete -f sockperf.yaml
#sleep 20
#echo "Done delete Pod"

#####################################################################################################################
