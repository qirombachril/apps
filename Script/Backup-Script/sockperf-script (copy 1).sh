CNI=sona
TIME=10
TEST=10
WORKER1=ba-k8s-worker1
IP_HOST=10.230.230.11
CLIENTS=$(kubectl get pods -l app=sockperf-client -o name | cut -d'/' -f2)
HOST=$(kubectl get pods -l app=sockperf-server -o name | cut -d'/' -f2)
IP_SOCKPERF_SRV=$(kubectl get pod ${HOST} -o jsonpath='{.status.podIP}')

echo "Check Pod Host"
for POD in ${CLIENTS}; do
CHECK=$(kubectl get po $POD -o jsonpath='{.spec.nodeName}')
    if [ "$CHECK" == "$WORKER1" ]; then 
        NODE_WORKER1=$POD
    else
        NODE_WORKER2=$POD
    fi
done

###################################################################
FILE="int_node_latency_h-t-p_$CNI"

echo  "Start Test Latency host to pod"
for (( c=1 ; c<=$TEST ; c++))
do  
    sockperf ping-pong --tcp -i $IP_SOCKPERF_SRV --msg-size 16 -p 11111 -t $TIME >> $FILE
done
echo  "Done Test host to pod"

echo "Create CSV latency host to pod"
echo -n "$CNI " >> csv-$FILE.csv
awk -F ' ' '/Summary:/{print $5}' $FILE | xargs echo >> csv-$FILE.csv 
echo "Done Create CSV"

###################################################################
sockperf server --tcp -p 11111 &

FILE="int_node_latency_p-t-h_$CNI"

echo  "Start Test Latency pod to host"
for (( c=1 ; c<=$TEST ; c++))
do  
    kubectl exec -it $NODE_WORKER1 -- sockperf ping-pong --tcp -i $IP_HOST --msg-size 16 -p 11111 -t $TIME >> $FILE
done
echo  "Done Test pod to host"

echo "Create CSV latency pod to host"
echo -n "$CNI " >> csv-$FILE.csv
awk -F ' ' '/Summary:/{print $5}' $FILE | xargs echo >> csv-$FILE.csv 
echo "Done Create CSV"

PS=$(ps aux | grep -v "grep" | grep "sockperf server" | awk 'NR==1{print $2}')
kill -9 $PS

###################################################################
FILE="dif_node_latency_tcp_$CNI"

echo  "Start Test Latency Pod different host"
for (( c=1 ; c<=$TEST ; c++))
do  
    kubectl exec -it $NODE_WORKER1 -- sockperf ping-pong --tcp -i $IP_SOCKPERF_SRV --msg-size 16 -p 11111 -t $TIME >> $FILE
done
echo  "Done Test Pod different host"

echo "Create CSV latency different host"
echo -n "$CNI " >> csv-$FILE.csv
awk -F ' ' '/Summary:/{print $5}' $FILE | xargs echo >> csv-$FILE.csv 
echo "Done Create CSV"
###################################################################
FILE="same_node_latency_tcp_$CNI"

echo  "Start Test Latency Pod same host"
for (( c=1 ; c<=$TEST ; c++))
do  
    kubectl exec -it $NODE_WORKER2 -- sockperf ping-pong --tcp -i $IP_SOCKPERF_SRV --msg-size 16  -p 11111 -t $TIME >> $FILE
done
echo  "Done Test Latenct Pod same host"

echo "Create CSV latency same host"
echo -n "$CNI " >> csv-$FILE.csv
awk -F ' ' '/Summary:/{print $5}' $FILE | xargs echo >> csv-$FILE.csv 
echo "Done Create CSV"
###################################################################

