CNI=sona
TIME=10
TEST=10
WORKER1=ba-k8s-worker1
BANDWIDTH=1000
IP_HOST=10.230.230.11
IP_HOST_EX=10.230.230.14
CLIENTS=$(ssh $IP_HOST '( kubectl get pods -l app=iperf3-client -o name | cut -d'/' -f2 )')
HOST=$(ssh $IP_HOST '( kubectl get pods -l app=iperf3-server -o name | cut -d'/' -f2 )')
IP_HOST_KUBE=$(ssh $IP_HOST "( kubectl get pod ${HOST} -o jsonpath='{.status.podIP}' )")

echo "Check Pod Host"
for POD in ${CLIENTS}; do
    CHECK=$( ssh $IP_HOST "( kubectl get po $POD -o jsonpath='{.spec.nodeName}' )")
    if [ "$CHECK" == "$WORKER1" ]; then
        NODE_WORKER1=$POD
        echo "1"
    else
        NODE_WORKER2=$POD
    fi
done
echo "Done Check Pod Host"
###################################################################
echo "2"
FILE="ex_node_th_tcp_h-t-p_$CNI"

echo  "Start Test Throughput ex network to pod"
for (( c=1 ; c<=$TEST ; c++))
do
    iperf3 -c $IP_HOST -p 30001 -t $TIME >> $FILE
done
echo  "Done Test ex network to pod"

echo "Create CSV ex network to pod"
echo -n "$CNI " >> csv-$FILE.csv
grep receiver $FILE | awk -F ' ' '{print $7}' | xargs echo >> csv-$FILE.csv
echo "Done Create CSV"


###################################################################

FILE="ex_node_th_tcp_p-t-h_$CNI"

iperf3 -s &

echo  "Start Test Throughput ex network to host"
for (( c=1 ; c<=$TEST ; c++))
do
    ssh $IP_HOST "( kubectl exec -it $NODE_WORKER1 -- iperf3 -c $IP_HOST_EX -t $TIME )" >> $FILE
done
echo  "Done Test ex network to host"

echo "Create CSV Throughput ex network to host"
echo -n "$CNI " >> csv-$FILE.csv
grep receiver $FILE | awk -F ' ' '{print $7}' | xargs echo >> csv-$FILE.csv
echo "Done Create CSV"


###################################################################
FILE="ex_node_packet_loss_h-t-p_$CNI"

echo  "Start Test ex node Packet Loss"
for (( c=1 ; c<=$TEST ; c++))
do  
    iperf3 -c $IP_HOST -u -p 30001 -b ${BANDWIDTH}m -t $TIME >> $FILE
done
echo  "Done Test ex node packet loss"

echo "Create CSV Packet Loss different host"
echo -n "$CNI " >> csv-$FILE.csv
A=$(grep receiver $FILE | awk -F ' ' '{print $11}' | awk '{split($0,a,"/"); print a[1]}')
B=$(grep receiver $FILE | awk -F ' ' '{print $11}' | awk '{split($0,a,"/"); print a[2]}')
X=100
set -f
IFS='
'
set -- $A
for i in ${B}
do
  awk "BEGIN {print $1/$i*$X}" | xargs echo -n >> csv-$FILE.csv
  echo -n " " >> csv-$FILE.csv
  shift
done
echo "" >> csv-$FILE.csv
echo "Done Create CSV"

###################################################################
FILE="ex_node_packet_loss_p-t-h_$CNI"

echo  "Start Test ex node Packet Loss "
for (( c=1 ; c<=$TEST ; c++))
do  
    ssh $IP_HOST "( kubectl exec -it $NODE_WORKER1 -- iperf3 -c $IP_HOST_EX -u -b ${BANDWIDTH}m -t $TIME )" >> $FILE
done
echo  "Done Test ex node Packet Loss"

echo "Create CSV Packet Loss different host"
echo -n "$CNI " >> csv-$FILE.csv
A=$(grep receiver $FILE | awk -F ' ' '{print $11}' | awk '{split($0,a,"/"); print a[1]}')
B=$(grep receiver $FILE | awk -F ' ' '{print $11}' | awk '{split($0,a,"/"); print a[2]}')
X=100
set -f
IFS='
'
set -- $A
for i in ${B}
do
  awk "BEGIN {print $1/$i*$X}" | xargs echo -n >> csv-$FILE.csv
  echo -n " " >> csv-$FILE.csv
  shift
done
echo "" >> csv-$FILE.csv
echo "Done Create CSV"

PS=$(ps aux | grep -v "grep" | grep "iperf3 -s" | awk 'NR==1{print $2}')
kill -9 $PS 

###################################################################

CLIENTS=$(ssh $IP_HOST '( kubectl get pods -l app=sockperf-client -o name | cut -d'/' -f2 )')
HOST=$(ssh $IP_HOST '( kubectl get pods -l app=sockperf-server -o name | cut -d'/' -f2 )')
IP_HOST_KUBE=$(ssh $IP_HOST "( kubectl get pod ${HOST} -o jsonpath='{.status.podIP}' )")

echo "Check Pod Host"
for POD in ${CLIENTS}; do
    CHECK=$( ssh $IP_HOST "( kubectl get po $POD -o jsonpath='{.spec.nodeName}' )")
    if [ "$CHECK" == "$WORKER1" ]; then
        NODE_WORKER1=$POD
    else
        NODE_WORKER2=$POD
    fi
done

FILE="ex_node_latency_tcp_h-t-p_$CNI"
echo  "Start Test ex node Latency "
for (( c=1 ; c<=$TEST ; c++))
do  
    sockperf ping-pong -i $IP_HOST -p 30003 --tcp --msg-size 16 -t $TIME >> $FILE
done
echo  "Done Test ex node Latency "

echo "Create CSV ex node Latency "
echo -n "$CNI " >> csv-$FILE.csv
awk -F ' ' '/Summary:/{print $5}' $FILE | xargs echo >> csv-$FILE.csv 
echo "Done Create CSV"

###################################################################
sockperf server --tcp -p 11111 &

FILE="ex_node_latency_tcp_p-t-h_$CNI"
echo  "Start Test Latency Pod different host"
for (( c=1 ; c<=$TEST ; c++))
do  
    ssh $IP_HOST "( kubectl exec -it $NODE_WORKER1 -- sockperf ping-pong --tcp -i $IP_HOST_EX --msg-size 16 -p 11111 -t $TIME )" >> $FILE
done
echo  "Done Test Pod different host"

echo "Create CSV latency different host"
echo -n "$CNI " >> csv-$FILE.csv
awk -F ' ' '/Summary:/{print $5}' $FILE | xargs echo >> csv-$FILE.csv 
echo "Done Create CSV"

PS=$(ps aux | grep -v "grep" | grep "sockperf server" | awk 'NR==1{print $2}')
kill -9 $PS

