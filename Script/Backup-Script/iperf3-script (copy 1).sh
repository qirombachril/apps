CNI=sona
TIME=10
TEST=10
BANDWIDTH=1000
WORKER1=ba-k8s-worker1
IP_HOST=10.230.230.11
CLIENTS=$(kubectl get pods -l app=iperf3-client -o name | cut -d'/' -f2)
HOST=$(kubectl get pods -l app=iperf3-server -o name | cut -d'/' -f2)
IP_IPERF_SRV=$(kubectl get pod ${HOST} -o jsonpath='{.status.podIP}')

echo "Check Pod Host"
for POD in ${CLIENTS}; do
CHECK=$(kubectl get po $POD -o jsonpath='{.spec.nodeName}')
    if [ "$CHECK" == "$WORKER1" ]; then 
        NODE_WORKER1=$POD
    else
        NODE_WORKER2=$POD
    fi
done

###################################################################
#Test Throughput int node to Pod#
#################################
FILE="int_node_th_tcp_h-t-p_$CNI"

echo  "Start Test Throughput int node to Pod"
for (( c=1 ; c<=$TEST ; c++))
do  
    iperf3 -c $IP_IPERF_SRV  -t $TIME >> $FILE
done
echo  "Done Test int node to Pod"

echo "Create CSV int node to Pod"
echo -n "$CNI " >> csv-$FILE.csv
grep receiver $FILE | awk -F ' ' '{print $7}' | xargs echo >> csv-$FILE.csv 
echo "Done Create CSV"



###################################################################
#Test Throughput pod to int node#
#################################
FILE="int_node_th_tcp_p-t-h_$CNI"

iperf3 -s &

echo  "Start Test Throughput pod to int node"
for (( c=1 ; c<=$TEST ; c++))
do
    kubectl exec -it $NODE_WORKER1 -- iperf3 -c $IP_HOST -t $TIME  >> $FILE
done
echo  "Done Test pod to int node"

echo "Create CSV Throughput pod to int node"
echo -n "$CNI " >> csv-$FILE.csv
grep receiver $FILE | awk -F ' ' '{print $7}' | xargs echo >> csv-$FILE.csv
echo "Done Create CSV"



###################################################################
#Test Throughput Pod different host#
#####################################
FILE="dif_node_th_tcp_$CNI"

echo  "Start Test Throughput Pod different host"
for (( c=1 ; c<=$TEST ; c++))
do  
    kubectl exec -it $NODE_WORKER1 -- iperf3 -c $IP_IPERF_SRV -t $TIME >> $FILE
done
echo  "Done Test Pod different host"

echo "Create CSV Throughput different host"
echo -n "$CNI " >> csv-$FILE.csv
grep receiver $FILE | awk -F ' ' '{print $7}' | xargs echo >> csv-$FILE.csv 
echo "Done Create CSV"

###################################################################
#Test Throughput Pod same host#
###############################

FILE="same_node_th_tcp_$CNI"
echo "Start Test Throughput Pod same host"
for (( c=1 ; c<=$TEST ; c++))
do  
    kubectl exec -it $NODE_WORKER2 -- iperf3 -c $IP_IPERF_SRV -t $TIME >> $FILE
done
echo  "Done Test Pod same host"

echo "Create CSV Throughput same host"
echo -n "$CNI " >> csv-$FILE.csv
grep receiver $FILE | awk -F ' ' '{print $7}' | xargs echo >> csv-$FILE.csv 
echo "Done Create CSV"

###################################################################
#Test Packet Loss int node to pod#
##################################

FILE="int_node_packet_loss_h-t-p_$CNI"

echo  "Start Test Packet Loss int node to pod"
for (( c=1 ; c<=$TEST ; c++))
do  
    iperf3 -c $IP_IPERF_SRV -u -b ${BANDWIDTH}m -t $TIME >> $FILE
done
echo  "Done Test Packet Loss int node to pod"

echo "Create CSV Test Packet Loss int node to pod"
echo -n "$CNI " >> csv-$FILE.csv
A=$(grep receiver $FILE | awk -F ' ' '{print $11}' | awk '{split($0,a,"/"); print a[1]}')
B=$(grep receiver $FILE | awk -F ' ' '{print $11}' | awk '{split($0,a,"/"); print a[2]}')
X=100
set -f
IFS='
'
set -- $A
for i in ${B}
do
  awk "BEGIN {print $1/$i*$X}" | xargs echo -n >> csv-$FILE.csv
  echo -n " " >> csv-$FILE.csv
  shift
done
echo "" >> csv-$FILE.csv
echo "Done Create CSV"

###################################################################
#Test Packet Loss pod to int node#
##################################

FILE="int_node_packet_loss_p-t-h_$CNI"

echo  "Start Test Packet Loss pod to int node"
for (( c=1 ; c<=$TEST ; c++))
do  
    kubectl exec -it $NODE_WORKER1 -- iperf3 -c $IP_HOST -u -b ${BANDWIDTH}m -t $TIME >> $FILE
done
echo  "Done Test Packet Loss pod to int node"

echo "Create CSV Packet Loss pod to int node"
echo -n "$CNI " >> csv-$FILE.csv
A=$(grep receiver $FILE | awk -F ' ' '{print $11}' | awk '{split($0,a,"/"); print a[1]}')
B=$(grep receiver $FILE | awk -F ' ' '{print $11}' | awk '{split($0,a,"/"); print a[2]}')
X=100
set -f
IFS='
'
set -- $A
for i in ${B}
do
  awk "BEGIN {print $1/$i*$X}" | xargs echo -n >> csv-$FILE.csv
  echo -n " " >> csv-$FILE.csv
  shift
done
echo "" >> csv-$FILE.csv
echo "Done Create CSV"

PS=$(ps aux | grep -v "grep" | grep "iperf3 -s" | awk 'NR==1{print $2}')
kill -9 $PS 

###################################################################
#Test Packet Loss Pod different host#
#####################################


FILE="dif_node_packet_loss_$CNI"

echo  "Start Test Packet Loss Pod different host"
for (( c=1 ; c<=$TEST ; c++))
do  
    kubectl exec -it $NODE_WORKER1 -- iperf3 -c $IP_IPERF_SRV -u -b ${BANDWIDTH}m -t $TIME >> $FILE
done
echo  "Done Test Pod different host"

echo "Create CSV Packet Loss different host"
echo -n "$CNI " >> csv-$FILE.csv
A=$(grep receiver $FILE | awk -F ' ' '{print $11}' | awk '{split($0,a,"/"); print a[1]}')
B=$(grep receiver $FILE | awk -F ' ' '{print $11}' | awk '{split($0,a,"/"); print a[2]}')
X=100
set -f
IFS='
'
set -- $A
for i in ${B}
do
  awk "BEGIN {print $1/$i*$X}" | xargs echo -n >> csv-$FILE.csv
  echo -n " " >> csv-$FILE.csv
  shift
done
echo "" >> csv-$FILE.csv
echo "Done Create CSV"

###################################################################
#Test Packet Loss Pod same host#
################################

FILE="same_node_packet_loss_$CNI"

echo  "Start Test Packet Loss Pod same host"
for (( c=1 ; c<=$TEST ; c++))
do  
    kubectl exec -it $NODE_WORKER2 -- iperf3 -c $IP_IPERF_SRV -u -b ${BANDWIDTH}m -t $TIME >> $FILE
done
echo  "Done Test Pod same host"

echo "Create CSV Packet Loss same host"
echo -n "$CNI " >> csv-$FILE.csv
A=$(grep receiver $FILE | awk -F ' ' '{print $11}' | awk '{split($0,a,"/"); print a[1]}')
B=$(grep receiver $FILE | awk -F ' ' '{print $11}' | awk '{split($0,a,"/"); print a[2]}')
X=100
set -f
IFS='
'
set -- $A
for i in ${B}
do
  awk "BEGIN {print $1/$i*$X}" | xargs echo -n >> csv-$FILE.csv
  echo -n " " >> csv-$FILE.csv
  shift
done
echo "" >> csv-$FILE.csv
echo "Done Create CSV"
