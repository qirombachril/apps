#!/bin/bash

CNI=sona-gre
TIME=90
TEST=1
BANDWIDTH=50
WORKER1=ba-k8s-worker1
IP_HOST=10.50.50.10
CLIENTS=$(kubectl get pods -l app=iperf3-client -o name | cut -d'/' -f2)
HOST=$(kubectl get pods -l app=iperf3-server -o name | cut -d'/' -f2)
IP_IPERF_SRV=$(kubectl get pod ${HOST} -o jsonpath='{.status.podIP}')

echo "Check Pod Host"
for POD in ${CLIENTS}; do
CHECK=$(kubectl get po $POD -o jsonpath='{.spec.nodeName}')
    if [ "$CHECK" == "$WORKER1" ]; then 
        NODE_WORKER1=$POD
    else
        NODE_WORKER2=$POD
    fi
done


##################################################################
#Test Throughput Pod different host#
####################################
FILE="dif_node_th_tcp_$CNI"

echo  "Start Test Throughput Pod different host"
sar -u 1 >> ${FILE}_CPU &
free -s 1 >> ${FILE}_RAM &

for (( c=1 ; c<=$TEST ; c++))
do  
    kubectl exec -it $NODE_WORKER1 -- iperf3 -c $IP_IPERF_SRV -t $TIME >> $FILE
done
echo  "Done Test Pod different host"

kill -9 $(pidof sar)
kill -9 $(pidof free)

echo -n "$CNI " >> csv-${FILE}_CPU.csv
CPU=$(grep all ${FILE}_CPU | awk -F ' ' '{print 100 - $8}')
echo $CPU | xargs echo >> csv-${FILE}_CPU.csv

echo -n "$CNI " >> csv-${FILE}_RAM.csv
RAM=$(grep Mem ${FILE}_RAM | awk '{print $3/$2 * 100.0}')
echo $RAM | xargs echo >> csv-${FILE}_RAM.csv


echo "Create CSV Throughput different host"
echo -n "$CNI " >> csv-$FILE.csv
sed -n -e '/Connecting/,/ -/ p' $FILE | awk -F'[ -]+' '/sec/{print $8}' | xargs echo >> csv-$FILE.csv
#grep receiver $FILE | awk -F ' ' '{print $7}' | xargs echo >> csv-$FILE.csv 
echo "Done Create CSV"

sed -i 's/ *$//' csv-$FILE.csv
###################################################################
#Test Throughput Pod same host#
###############################

FILE="same_node_th_tcp_$CNI"
echo "Start Test Throughput Pod same host"
sar -u 1 >> ${FILE}_CPU &
free -s 1 >> ${FILE}_RAM &

for (( c=1 ; c<=$TEST ; c++))
do  
    kubectl exec -it $NODE_WORKER2 -- iperf3 -c $IP_IPERF_SRV -t $TIME >> $FILE
done
echo  "Done Test Pod same host"

kill -9 $(pidof sar)
kill -9 $(pidof free)

echo -n "$CNI " >> csv-${FILE}_CPU.csv
CPU=$(grep all ${FILE}_CPU | awk -F ' ' '{print 100 - $8}')
echo $CPU | xargs echo >> csv-${FILE}_CPU.csv

echo -n "$CNI " >> csv-${FILE}_RAM.csv
RAM=$(grep Mem ${FILE}_RAM | awk '{print $3/$2 * 100.0}')
echo $RAM | xargs echo >> csv-${FILE}_RAM.csv


echo "Create CSV Throughput same host"
echo -n "$CNI " >> csv-$FILE.csv
sed -n -e '/Connecting/,/ -/ p' $FILE | awk -F'[ -]+' '/sec/{print $8}' | xargs echo >> csv-$FILE.csv
#grep receiver $FILE | awk -F ' ' '{print $7}' | xargs echo >> csv-$FILE.csv 
echo "Done Create CSV"

sed -i 's/ *$//' csv-$FILE.csv

func() {
##################################################################
#Test Throughput Pod different host#
####################################
FILE="dif_node_th_udp_$CNI"

echo  "Start Test Throughput Pod different host"
sar -u 1 >> ${FILE}_CPU &
free -s 1 >> ${FILE}_RAM &

for (( c=1 ; c<=$TEST ; c++))
do  
    kubectl exec -it $NODE_WORKER1 -- iperf3 -c $IP_IPERF_SRV -u -b ${1}m -t $TIME >> $FILE
done
echo  "Done Test Pod different host"

kill -9 $(pidof sar)
kill -9 $(pidof free)

echo -n "$CNI " >> csv-${FILE}_CPU.csv
CPU=$(grep all ${FILE}_CPU | awk -F ' ' '{print 100 - $8}')
echo $CPU | xargs echo >> csv-${FILE}_CPU.csv

echo -n "$CNI " >> csv-${FILE}_RAM.csv
RAM=$(grep Mem ${FILE}_RAM | awk '{print $3/$2 * 100.0}')
echo $RAM | xargs echo >> csv-${FILE}_RAM.csv


echo "Create CSV Throughput different host"
echo -n "$CNI " >> csv-$FILE.csv
sed -n -e '/Connecting/,/ -/ p' $FILE | awk -F'[ -]+' '/sec/{print $8}' | xargs echo >> csv-$FILE.csv
#grep receiver $FILE | awk -F ' ' '{print $7}' | xargs echo >> csv-$FILE.csv 
echo "Done Create CSV"

sed -i 's/ *$//' csv-$FILE.csv
###################################################################
#Test Throughput Pod same host#
###############################

FILE="same_node_th_udp_$CNI"
echo "Start Test Throughput Pod same host"
sar -u 1 >> ${FILE}_CPU &
free -s 1 >> ${FILE}_RAM &

for (( c=1 ; c<=$TEST ; c++))
do  
    kubectl exec -it $NODE_WORKER2 -- iperf3 -c $IP_IPERF_SRV -u -b ${1}m -t $TIME >> $FILE
done
echo  "Done Test Pod same host"

kill -9 $(pidof sar)
kill -9 $(pidof free)

echo -n "$CNI " >> csv-${FILE}_CPU.csv
CPU=$(grep all ${FILE}_CPU | awk -F ' ' '{print 100 - $8}')
echo $CPU | xargs echo >> csv-${FILE}_CPU.csv

echo -n "$CNI " >> csv-${FILE}_RAM.csv
RAM=$(grep Mem ${FILE}_RAM | awk '{print $3/$2 * 100.0}')
echo $RAM | xargs echo >> csv-${FILE}_RAM.csv


echo "Create CSV Throughput same host"
echo -n "$CNI " >> csv-$FILE.csv
sed -n -e '/Connecting/,/ -/ p' $FILE | awk -F'[ -]+' '/sec/{print $8}' | xargs echo >> csv-$FILE.csv
#grep receiver $FILE | awk -F ' ' '{print $7}' | xargs echo >> csv-$FILE.csv 
echo "Done Create CSV"

sed -i 's/ *$//' csv-$FILE.csv

###################################################################
#Test Packet Loss Pod different host#
#####################################


FILE="dif_node_packet_loss_$CNI"

echo  "Start Test Packet Loss Pod different host"
sar -u 1 >> ${FILE}_CPU &
free -s 1 >> ${FILE}_RAM &

for (( c=1 ; c<=$TEST ; c++))
do  
    kubectl exec -it $NODE_WORKER1 -- iperf3 -c $IP_IPERF_SRV -u -b ${1}m -t $TIME --get-server-output >> $FILE
done
echo  "Done Test Pod different host"

kill -9 $(pidof sar)
kill -9 $(pidof free)

echo -n "$CNI " >> csv-${FILE}_CPU.csv
CPU=$(grep all ${FILE}_CPU | awk -F ' ' '{print 100 - $8}')
echo $CPU | xargs echo >> csv-${FILE}_CPU.csv

echo -n "$CNI " >> csv-${FILE}_RAM.csv
RAM=$(grep Mem ${FILE}_RAM | awk '{print $3/$2 * 100.0}')
echo $RAM | xargs echo >> csv-${FILE}_RAM.csv


echo "Create CSV Packet Loss different host"
echo -n "$CNI " >> csv-$FILE.csv
A=$(sed -n -e '/Server/,/ --/ p' $FILE | awk -F'[ -]+' '/sec/{print $12}' | awk '{split($0,a,"/"); print a[1]}')
B=$(sed -n -e '/Server/,/ --/ p' $FILE | awk -F'[ -]+' '/sec/{print $12}' | awk '{split($0,a,"/"); print a[2]}')

X=100
set -f
IFS='
'
set -- $A
for i in ${B}
do
  awk "BEGIN {print $1/$i*$X}" | xargs echo -n >> csv-$FILE.csv
  echo -n " " >> csv-$FILE.csv
  shift
done
echo "" >> csv-$FILE.csv
echo "Done Create CSV"
sed -i 's/ *$//' csv-$FILE.csv

###################################################################
#Test Packet Loss Pod same host#
################################

FILE="same_node_packet_loss_$CNI"

echo  "Start Test Packet Loss Pod same host"
sar -u 1 >> ${FILE}_CPU &
free -s 1 >> ${FILE}_RAM &

for (( c=1 ; c<=$TEST ; c++))
do  
    kubectl exec -it $NODE_WORKER2 -- iperf3 -c $IP_IPERF_SRV -u -b ${1}m -t $TIME --get-server-output >> $FILE
done
echo  "Done Test Pod same host"

kill -9 $(pidof sar)
kill -9 $(pidof free)

echo -n "$CNI " >> csv-${FILE}_CPU.csv
CPU=$(grep all ${FILE}_CPU | awk -F ' ' '{print 100 - $8}')
echo $CPU | xargs echo >> csv-${FILE}_CPU.csv

echo -n "$CNI " >> csv-${FILE}_RAM.csv
RAM=$(grep Mem ${FILE}_RAM | awk '{print $3/$2 * 100.0}')
echo $RAM | xargs echo >> csv-${FILE}_RAM.csv


echo "Create CSV Packet Loss same host"
echo -n "$CNI " >> csv-$FILE.csv
A=$(sed -n -e '/Server/,/ --/ p' $FILE | awk -F'[ -]+' '/sec/{print $12}' | awk '{split($0,a,"/"); print a[1]}')
B=$(sed -n -e '/Server/,/ --/ p' $FILE | awk -F'[ -]+' '/sec/{print $12}' | awk '{split($0,a,"/"); print a[2]}')
X=100
set -f
IFS='
'
set -- $A
for i in ${B}
do
  awk "BEGIN {print $1/$i*$X}" | xargs echo -n >> csv-$FILE.csv
  echo -n " " >> csv-$FILE.csv
  shift
done
echo "" >> csv-$FILE.csv
echo "Done Create CSV"
sed -i 's/ *$//' csv-$FILE.csv

}

for k in 50 100 200 300 ; do 
mkdir $k
func $k
UDP=$(ls | grep udp)
LO=$(ls | grep packet_loss)
mv $UDP $LO $k
done