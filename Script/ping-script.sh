ram-check() {
echo "      time $(free -m | grep total | sed -E 's/^    (.*)/\1/g')"
while true; do
    echo "$(date '+%H:%M:%S') $(free | grep Mem: | sed 's/Mem://g')"
    sleep 1
done
}

CNI=flannel
TIME=0
TEST=0
WORKER1=ba-k8s-cni-worker1
IP_HOST=10.50.50.20
CLIENTS=$(kubectl get pods -l app=sockperf-client -o name | cut -d'/' -f2)
HOST=$(kubectl get pods -l app=sockperf-server -o name | cut -d'/' -f2)
IP_SOCKPERF_SRV=$(kubectl get pod ${HOST} -o jsonpath='{.status.podIP}')

echo "Check Pod Host"
for POD in ${CLIENTS}; do
CHECK=$(kubectl get po $POD -o jsonpath='{.spec.nodeName}')
    if [ "$CHECK" == "$WORKER1" ]; then 
        NODE_WORKER1=$POD
    else
        NODE_WORKER2=$POD
    fi
done


###################################################################
FILE="dif_node_latency_tcp_$CNI"

echo  "Start Test Latency Pod different host"

sleep 1
for (( c=1 ; c<=$TEST ; c++))
do  
    kubectl exec -it $NODE_WORKER1 -- ping -i 1 -c $TIME $IP_SOCKPERF_SRV >> $FILE
done
sleep 1

echo  "Done Test Pod different host"

echo "Create CSV latency different host"
echo -n "$CNI " >> csv-$FILE.csv
awk -F' ' '/time=/{print $7}' $FILE | awk -F'=' '{print $2}' | xargs echo >> csv-$FILE.csv 
echo "Done Create CSV"
###################################################################
FILE="same_node_latency_tcp_$CNI"

echo  "Start Test Latency Pod same host"

sleep 1
for (( c=1 ; c<=$TEST ; c++))
do  
    kubectl exec -it $NODE_WORKER2 -- ping -i 1 -c $TIME $IP_SOCKPERF_SRV >> $FILE
done
echo  "Done Test Latenct Pod same host"
sleep 1

echo "Create CSV latency same host"
echo -n "$CNI " >> csv-$FILE.csv
awk -F' ' '/time=/{print $7}' $FILE | awk -F'=' '{print $2}' | xargs echo >> csv-$FILE.csv 
echo "Done Create CSV"
###################################################################
