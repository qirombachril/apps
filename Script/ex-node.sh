ram-check() {
echo "      time $(free -m | grep total | sed -E 's/^    (.*)/\1/g')"
while true; do
    echo "$(date '+%H:%M:%S') $(free | grep Mem: | sed 's/Mem://g')"
    sleep 1
done
}

CNI=calico
TIME=180
TEST=1
WORKER1=ba-k8s-cni-worker1
BANDWIDTH=100
IP_HOST=10.50.50.20
IP_HOST_EX=10.50.50.13
CLIENTS=$(ssh $IP_HOST '( kubectl get pods -l app=iperf3-client -o name | cut -d'/' -f2 )')
HOST=$(ssh $IP_HOST '( kubectl get pods -l app=iperf3-server -o name | cut -d'/' -f2 )')
IP_HOST_KUBE=$(ssh $IP_HOST "( kubectl get pod ${HOST} -o jsonpath='{.status.podIP}' )")

echo "Check Pod Host"
for POD in ${CLIENTS}; do
    CHECK=$( ssh $IP_HOST "( kubectl get po $POD -o jsonpath='{.spec.nodeName}' )")
    if [ "$CHECK" == "$WORKER1" ]; then
        NODE_WORKER1=$POD
        echo "1"
    else
        NODE_WORKER2=$POD
    fi
done
echo "Done Check Pod Host"
###################################################################
echo "2"
FILE="ex_node_th_tcp_h-t-p_$CNI"

echo  "Start Test Throughput ex network to pod"
actionsTh() {
ssh $IP_HOST "( sar -u 1 )" >> ${FILE}_CPU &
ssh $IP_HOST "( free -s 1 )" >> ${FILE}_RAM &

sleep 1
for (( c=1 ; c<=$1 ; c++))
do
    iperf3 -c $IP_HOST -p 30001 -t $TIME >> $FILE
done

kill -9 $(pidof ssh)

echo -n "$CNI " >> csv-${FILE}_CPU.csv
CPU=$(grep all ${FILE}_CPU | awk -F ' ' '{print 100 - $8}')
echo $CPU | xargs echo >> csv-${FILE}_CPU.csv

echo -n "$CNI " >> csv-${FILE}_RAM.csv
RAM=$(grep Mem ${FILE}_RAM | awk '{print $3/$2 * 100.0}')
echo $RAM | xargs echo >> csv-${FILE}_RAM.csv

echo "Create CSV ex network to pod"
echo -n "$CNI " >> csv-$FILE.csv
sed -n -e '/Connecting/,/ -/ p' $FILE | awk -F'[ -]+' '/sec/{print $8}' | xargs echo >> csv-$FILE.csv
#grep receiver $FILE | awk -F ' ' '{print $7}' | xargs echo >> csv-$FILE.csv
echo "Done Create CSV"
}
actionsTh $TEST

##### Check
sed -i 's/ *$//' csv-$FILE.csv
#TOTAL=$(cat csv-$FILE.csv | grep -o " " | wc -l)

#while [ $TOTAL -lt $TEST ]; do
#        echo "Ulang Dulu Bos!"
#        rm csv-$FILE.csv csv-${FILE}_RAM.csv csv-${FILE}_CPU.csv
#        let "ULANG = $TEST - $TOTAL"
#        actionsTh $ULANG
#        sed -i 's/ *$//' csv-$FILE.csv
#        TOTAL=$(cat csv-$FILE.csv | grep -o " " | wc -l)
#done


###################################################################

FILE="ex_node_th_tcp_p-t-h_$CNI"

iperf3 -s &

echo  "Start Test Throughput ex network to host"
ssh $IP_HOST "( sar -u 1 )" >> ${FILE}_CPU &
ssh $IP_HOST "( free -s 1 )" >> ${FILE}_RAM &

sleep 1
for (( c=1 ; c<=$TEST ; c++))
do
    ssh $IP_HOST "( kubectl exec -it $NODE_WORKER1 -- iperf3 -c $IP_HOST_EX -t $TIME )" >> $FILE
done
echo  "Done Test ex network to host"

kill -9 $(pidof ssh)

echo -n "$CNI " >> csv-${FILE}_CPU.csv
CPU=$(grep all ${FILE}_CPU | awk -F ' ' '{print 100 - $8}')
echo $CPU | xargs echo >> csv-${FILE}_CPU.csv

echo -n "$CNI " >> csv-${FILE}_RAM.csv
RAM=$(grep Mem ${FILE}_RAM | awk '{print $3/$2 * 100.0}')
echo $RAM | xargs echo >> csv-${FILE}_RAM.csv

echo "Create CSV Throughput ex network to host"
echo -n "$CNI " >> csv-$FILE.csv
sed -n -e '/Connecting/,/ -/ p' $FILE | awk -F'[ -]+' '/sec/{print $8}' | xargs echo >> csv-$FILE.csv
#grep receiver $FILE | awk -F ' ' '{print $7}' | xargs echo >> csv-$FILE.csv
echo "Done Create CSV"
sed -i 's/ *$//' csv-$FILE.csv

###################################################################
FILE="ex_node_packet_loss_h-t-p_$CNI"

echo  "Start Test ex node Packet Loss"
actionsPacketLoss() {
ssh $IP_HOST "( sar -u 1 )" >> ${FILE}_CPU &
ssh $IP_HOST "( free -s 1 )" >> ${FILE}_RAM &

sleep 1
for (( c=1 ; c<=$1 ; c++))
do  
    iperf3 -c $IP_HOST -u -p 30001 -b ${BANDWIDTH}m -t $TIME --get-server-output >> $FILE
done
echo  "Done Test ex node packet loss"

kill -9 $(pidof ssh)

echo -n "$CNI " >> csv-${FILE}_CPU.csv
CPU=$(grep all ${FILE}_CPU | awk -F ' ' '{print 100 - $8}')
echo $CPU | xargs echo >> csv-${FILE}_CPU.csv

echo -n "$CNI " >> csv-${FILE}_RAM.csv
RAM=$(grep Mem ${FILE}_RAM | awk '{print $3/$2 * 100.0}')
echo $RAM | xargs echo >> csv-${FILE}_RAM.csv

echo "Create CSV Packet Loss different host"
echo -n "$CNI " >> csv-$FILE.csv
A=$(sed -n -e '/Server/,/ -/ p' $FILE | awk -F'[ -]+' '/sec/{print $12}' | awk '{split($0,a,"/"); print a[1]}')
B=$(sed -n -e '/Server/,/ -/ p' $FILE | awk -F'[ -]+' '/sec/{print $12}' | awk '{split($0,a,"/"); print a[2]}')

#A=$(grep receiver $FILE | awk -F ' ' '{print $11}' | awk '{split($0,a,"/"); print a[1]}')
#B=$(grep receiver $FILE | awk -F ' ' '{print $11}' | awk '{split($0,a,"/"); print a[2]}')
X=100
set -f
IFS='
'
set -- $A
for i in ${B}
do
  awk "BEGIN {print $1/$i*$X}" | xargs echo -n >> csv-$FILE.csv
  echo -n " " >> csv-$FILE.csv
  shift
done
echo "" >> csv-$FILE.csv
echo "Done Create CSV"
}
actionsPacketLoss $TEST

##### Check
sed -i 's/ *$//' csv-$FILE.csv
#TOTAL=$(cat csv-$FILE.csv | grep -o " " | wc -l)

#while [ $TOTAL -lt $TEST ]; do
#        echo "Ulang Dulu Bos!"
#        rm csv-$FILE.csv csv-${FILE}_RAM.csv csv-${FILE}_CPU.csv
#        let "ULANG = $TEST - $TOTAL"
#        actionsPacketLoss $ULANG
#        sed -i 's/ *$//' csv-$FILE.csv
#        TOTAL=$(cat csv-$FILE.csv | grep -o " " | wc -l)
#done


###################################################################
FILE="ex_node_packet_loss_p-t-h_$CNI"

echo  "Start Test ex node Packet Loss "
ssh $IP_HOST "( sar -u 1 )" >> ${FILE}_CPU &
ssh $IP_HOST "( free -s 1 )" >> ${FILE}_RAM &

sleep 1
for (( c=1 ; c<=$TEST ; c++))
do  
    ssh $IP_HOST "( kubectl exec -it $NODE_WORKER1 -- iperf3 -c $IP_HOST_EX -u -b ${BANDWIDTH}m -t $TIME --get-server-output )" >> $FILE
done

echo  "Done Test ex node Packet Loss"

kill -9 $(pidof ssh)

echo -n "$CNI " >> csv-${FILE}_CPU.csv
CPU=$(grep all ${FILE}_CPU | awk -F ' ' '{print 100 - $8}')
echo $CPU | xargs echo >> csv-${FILE}_CPU.csv

echo -n "$CNI " >> csv-${FILE}_RAM.csv
RAM=$(grep Mem ${FILE}_RAM | awk '{print $3/$2 * 100.0}')
echo $RAM | xargs echo >> csv-${FILE}_RAM.csv

echo "Create CSV Packet Loss different host"
echo -n "$CNI " >> csv-$FILE.csv
A=$(sed -n -e '/Server/,/ -/ p' $FILE | awk -F'[ -]+' '/sec/{print $12}' | awk '{split($0,a,"/"); print a[1]}')
B=$(sed -n -e '/Server/,/ -/ p' $FILE | awk -F'[ -]+' '/sec/{print $12}' | awk '{split($0,a,"/"); print a[2]}')

#A=$(grep receiver $FILE | awk -F ' ' '{print $11}' | awk '{split($0,a,"/"); print a[1]}')
#B=$(grep receiver $FILE | awk -F ' ' '{print $11}' | awk '{split($0,a,"/"); print a[2]}')
X=100
set -f
IFS='
'
set -- $A
for i in ${B}
do
  awk "BEGIN {print $1/$i*$X}" | xargs echo -n >> csv-$FILE.csv
  echo -n " " >> csv-$FILE.csv
  shift
done
echo "" >> csv-$FILE.csv
echo "Done Create CSV"
sed -i 's/ *$//' csv-$FILE.csv

PS=$(ps aux | grep -v "grep" | grep "iperf3 -s" | awk 'NR==1{print $2}')
kill -9 $PS 

####################################################################
TIME=0
TEST=0
CLIENTS=$(ssh $IP_HOST '( kubectl get pods -l app=sockperf-client -o name | cut -d'/' -f2 )')
HOST=$(ssh $IP_HOST '( kubectl get pods -l app=sockperf-server -o name | cut -d'/' -f2 )')
IP_HOST_KUBE=$(ssh $IP_HOST "( kubectl get pod ${HOST} -o jsonpath='{.status.podIP}' )")

echo "Check Pod Host"
for POD in ${CLIENTS}; do
    CHECK=$( ssh $IP_HOST "( kubectl get po $POD -o jsonpath='{.spec.nodeName}' )")
    if [ "$CHECK" == "$WORKER1" ]; then
        NODE_WORKER1=$POD
    else
        NODE_WORKER2=$POD
    fi
done

FILE="ex_node_latency_tcp_h-t-p_$CNI"
ssh $IP_HOST "( sar -u 1 )" >> ${FILE}_CPU &
ssh $IP_HOST "$(typeset -f ram-check); ram-check" >> ${FILE}_RAM &
rampid=$!

echo  "Start Test ex node Latency "

sleep 1
for (( c=1 ; c<=$TEST ; c++))
do  
    echo "start : $(date '+%H:%M:%S')" >> $FILE
    sockperf ping-pong -i $IP_HOST -p 30003 --tcp --msg-size 16 -t $TIME >> $FILE
    echo "end : $(date '+%H:%M:%S')" >> $FILE
done
echo  "Done Test ex node Latency "
sleep 1

kill -9 $(pidof ssh)
kill $rampid > /dev/null 2>&1

TempTime=$(grep "end :" $FILE | awk '{print $3}')

echo -n "$CNI" >> csv-${FILE}_CPU.csv
for i in $TempTime ; do
        input=$(grep $i ${FILE}_CPU | awk -F ' ' '{print 100 - $8}')
        echo -n " " >> csv-${FILE}_CPU.csv
        echo -n $input >> csv-${FILE}_CPU.csv
done

echo -n "$CNI" >> csv-${FILE}_RAM.csv
for i in $TempTime ; do
        input=$(grep $i ${FILE}_RAM | awk '{print $3/$2 * 100.0}')
        echo -n " " >> csv-${FILE}_RAM.csv
        echo -n $input >> csv-${FILE}_RAM.csv
done

echo "Create CSV ex node Latency "
echo -n "$CNI " >> csv-$FILE.csv
awk -F ' ' '/Summary:/{print $5}' $FILE | xargs echo >> csv-$FILE.csv 
echo "Done Create CSV"

###################################################################
sockperf server --tcp -p 11111 &

FILE="ex_node_latency_tcp_p-t-h_$CNI"
echo  "Start Test Latency Pod different host"
ssh $IP_HOST "( sar -u 1 )" >> ${FILE}_CPU &
ssh $IP_HOST "$(typeset -f ram-check); ram-check" >> ${FILE}_RAM &
rampid=$!

sleep 1
for (( c=1 ; c<=$TEST ; c++))
do  
    echo "start : $(date '+%H:%M:%S')" >> $FILE
    ssh $IP_HOST "( kubectl exec -it $NODE_WORKER1 -- sockperf ping-pong --tcp -i $IP_HOST_EX --msg-size 16 -p 11111 -t $TIME )" >> $FILE
    echo "end : $(date '+%H:%M:%S')" >> $FILE
done
echo  "Done Test Pod different host"
sleep 1

kill -9 $(pidof ssh)
kill $rampid > /dev/null 2>&1

TempTime=$(grep "end :" $FILE | awk '{print $3}')

echo -n "$CNI" >> csv-${FILE}_CPU.csv
for i in $TempTime ; do
        input=$(grep $i ${FILE}_CPU | awk -F ' ' '{print 100 - $8}')
        echo -n " " >> csv-${FILE}_CPU.csv
        echo -n $input >> csv-${FILE}_CPU.csv
done

echo -n "$CNI" >> csv-${FILE}_RAM.csv
for i in $TempTime ; do
        input=$(grep $i ${FILE}_RAM | awk '{print $3/$2 * 100.0}')
        echo -n " " >> csv-${FILE}_RAM.csv
        echo -n $input >> csv-${FILE}_RAM.csv
done

echo "Create CSV latency different host"
echo -n "$CNI " >> csv-$FILE.csv
awk -F ' ' '/Summary:/{print $5}' $FILE | xargs echo >> csv-$FILE.csv 
echo "Done Create CSV"

PS=$(ps aux | grep -v "grep" | grep "sockperf server" | awk 'NR==1{print $2}')
kill -9 $PS
